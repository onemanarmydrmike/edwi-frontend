import { GET_POSTS_URL } from 'src/ApiConstants'

export const GET_POSTS = 'GET_POSTS'
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS'
export const GET_POSTS_FAILED = 'GET_POSTS_FAILED'
export const FILTER_POSTS = 'FILTER_POSTS'

export function getPosts() {
  return {
    type: GET_POSTS
  }
}

export function getPostsSuccess(posts) {
  return {
    type: GET_POSTS_SUCCESS,
    posts: posts
  }
}

export function getPostsFailed(error) {
  return {
    type: GET_POSTS_FAILED,
    error: error
  }
}

export function fetchPosts() {
  return dispatch => {
    dispatch(getPosts())

    fetch(GET_POSTS_URL)
    .then(response => response.json())
    .then(json => {
      dispatch(getPostsSuccess(json))
    })
    .catch(error => {
      dispatch(getPostsFailed(error))
    })
  }
}

export function filterPosts(filterInput) {
  return {
    type: FILTER_POSTS,
    filterInput: filterInput
  }
}