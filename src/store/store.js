import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'

const initialState = {
  posts: [],
  filteredPosts: [],
  isDownloadingInProgress: false,
  filterInput: ''
}

const store = createStore(reducer, initialState, applyMiddleware(thunk))

export default store