import * as actions from './actions'

const reducer = (state, action) => {
  switch (action.type) {
    case actions.GET_POSTS:
      return {
        ...state,
        isDownloadingInProgress: true
      }
    case actions.GET_POSTS_SUCCESS:
      const { posts } = action
      return {
        ...state,
        isDownloadingInProgress: false,
        posts: posts,
        filteredPosts: filterPosts(posts, state.filterInput)
      }
    case actions.GET_POSTS_FAILED:
      return {
        ...state,
        isDownloadingInProgress: false,
        error: action.error
      }
    case actions.FILTER_POSTS:
      const { filterInput } = action
      return {
        ...state,
        filterInput: filterInput,
        filteredPosts: filterPosts(state.posts, filterInput)
      }
    default:
      return state
  }
}

function filterPosts(posts, filterInput) {
  const keyWords = filterInput.split(' ')
  return posts.filter(post => keyWords.every(keyWord => post.author.includes(keyWord) || post.body.includes(keyWord)))
}

export default reducer