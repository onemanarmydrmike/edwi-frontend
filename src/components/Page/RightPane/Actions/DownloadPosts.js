import React from 'react'
import { Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { connect } from 'react-redux'
import { fetchPosts } from 'src/store/actions'

const useStyles = makeStyles(() => ({
  root: {
    width: '80%',
    marginBottom: '2rem'
  }
}))

function DownloadPosts(props) {
  const classes = useStyles()
  return (
      <Button
          className={classes.root}
          variant='contained'
          color='primary'
          onClick={props.onClick}
      >
        Download Posts
      </Button>
  )
}

const mapStateToProps = state => state

const mapDispatcherToProps = dispatch => {
  return {
    onClick: () => dispatch(fetchPosts())
  }
}

export default connect(mapStateToProps, mapDispatcherToProps)(DownloadPosts)