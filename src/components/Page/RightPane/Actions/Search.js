import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { TextField } from '@material-ui/core'
import { filterPosts } from 'src/store/actions'
import { connect } from 'react-redux'

const useStyles = makeStyles(() => ({
  root: {
    width: '80%'
  },
  input: {
    fontSize: 40,
  },
  inputLabel: {
    fontSize: 30,
  }
}))

function Search(props) {
  const classes = useStyles()
  return (
      <TextField
          className={classes.root}
          label='Search'
          placeholder='key words...'
          onChange={evt => props.onInputChange(evt.target.value)}
          InputProps={{
            className: classes.input
          }}
          InputLabelProps={{
            className: classes.inputLabel
          }}
      />
  )
}

const mapStateToProps = state => state

const mapDispatcherToProps = dispatch => {
  return {
    onInputChange: (filterInput) => dispatch(filterPosts(filterInput))
  }
}

export default connect(mapStateToProps, mapDispatcherToProps)(Search)