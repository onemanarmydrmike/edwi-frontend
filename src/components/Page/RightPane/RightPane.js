import React from 'react'
import Search from './Actions/Search'
import DownloadPosts from './Actions/DownloadPosts'

function RightPane() {
  return (
      <div>
        <DownloadPosts/>
        <Search/>
      </div>
  )
}

export default RightPane