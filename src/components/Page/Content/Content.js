import React from 'react'
import { connect } from 'react-redux'
import { CircularProgress } from '@material-ui/core'
import { fetchPosts } from 'src/store/actions'
import Posts from './Posts/Posts'
import WykopLogo from '../../../resources/img/logo_wykop_250.png'

function Content(props) {
  const { isDownloadingInProgress, filteredPosts } = props

  return (
      <div style={{ height: '65vh' }}>
        <img src={WykopLogo} alt='logo wykop.pl' style={{ marginBottom: '4rem' }}/>
        {
          isDownloadingInProgress
              ? <CircularProgress/>
              : <Posts filteredPosts={filteredPosts}/>
        }
      </div>
  )
}

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => {
  return {
    fetchPosts: dispatch(fetchPosts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Content)