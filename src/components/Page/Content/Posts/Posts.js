import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { Grid } from '@material-ui/core/es/index'
import Scrollbar from 'react-custom-scrollbars'

const useStyles = makeStyles(() => ({
  post: {
    width: '80%',
    paddingLeft: '10%'
  },
  gridContainer: {
    marginBottom: '20px',
    borderBottom: 'dashed blue'
  },
  avatar: {
    textAlign: 'right',
    paddingRight: '2rem'
  },
  author: {
    textAlign: 'left',
    paddingLeft: '2rem'
  },
  body: {
    textAlign: 'left',
    padding: '2rem'
  }
}))

function Posts(props) {
  const { filteredPosts } = props

  const classes = useStyles()
  return (
      <Scrollbar>
        {
          filteredPosts
            .sort((a, b) => new Date(b.date) - new Date(a.date))
            .map(post => {
              return (
                  <div className={classes.post} key={`${post.author}-${post.date}-${post.body}`}>
                    <Grid container alignItems='center' className={classes.gridContainer}>
                      <Grid item xs={3} className={classes.avatar}>
                        <img src={post.avatar} alt='avatar'/>
                      </Grid>
                      <Grid item xs={9}>
                        <Grid item xs={12} className={classes.author}>
                          {post.author}, {post.date}
                        </Grid>
                        <Grid item xs={12} className={classes.body}>
                          <div dangerouslySetInnerHTML={{__html: post.body.replace(/(<? *script)/gi, 'illegalscript')}} />
                        </Grid>
                      </Grid>
                    </Grid>
                  </div>
              )
          })
        }
      </Scrollbar>
  )
}

export default Posts